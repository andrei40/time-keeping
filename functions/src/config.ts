import * as admin from "firebase-admin";
import * as serviceAccount from "./serviceKey.json";

const firebaseConfig = {
  apiKey: "AIzaSyBOI9LSsNN0vF_ONzLhAZCL-0LwZpsNrxs",
  authDomain: "time-keeping-6dc0c.firebaseapp.com",
  databaseURL: "https://time-keeping-6dc0c.firebaseio.com",
  projectId: "time-keeping-6dc0c",
  storageBucket: "time-keeping-6dc0c.appspot.com",
  messagingSenderId: "638060209534",
  appId: "1:638060209534:web:4cbc6e244f03f697e27f2f",
  measurementId: "G-LTJHJJTK1P",
  credential: admin.credential.cert(serviceAccount as any)
};

admin.initializeApp(firebaseConfig);
export const firestore = admin.firestore();
