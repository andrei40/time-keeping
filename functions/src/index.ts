import * as _ from "lodash";
import * as functions from "firebase-functions";
import fetch from "node-fetch";
import { firestore } from "./config";

const cors = require("cors")({ origin: true });

//todo: can be declare separatly to be reusable to models/interface/type
interface User {
  userUId?: string;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  isOnline: boolean;
}

export const CLOCK_API =
  "http://worldtimeapi.org/api/timezone/America/New_York";

export const getClockTime = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    fetch(CLOCK_API)
      .then(res => res.json())
      .then(data => {
        response.send(JSON.stringify(data));
      })
      .catch(err => console.log(err));
  });
});

// TODO: initial permission just to check user role and get it working, can be redesign to create a list of role
// plan of redesign: a list of role, then if user's role match the list of role return a true then may now allow the screen or route

export const checkUserRole = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    const result = await firestore
      .collection("roles")
      .where("userUId", "==", request.query.userUId)
      .get();

    if (result.docs.length > 0) {
      if (result.docs[0].exists) {
        response.status(200).json({ data: result.docs[0].data() });
      } else {
        response.status(400).json({ status: "error 400" });
      }
    } else {
      response.status(400).json({ status: "error 400" });
    }
  });
});

export const getEmployees = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    const role = await firestore
      .collection("roles")
      .where("role", "==", "employee")
      .get();

    if (role.docs.length > 0) {
      const userIds = _.filter(role.docs, user => user.exists).map(
        userRole => userRole.data().userUId
      );

      const userListQuery = await firestore.getAll(
        ...userIds.map(userId => {
          return firestore.collection("users").doc(userId);
        })
      );
      const userResult: User[] = [];
      userListQuery.forEach(userListItem => {
        if (userListItem.exists) {
          const user = userListItem.data();
          if (user) {
            userResult.push({
              userUId: userListItem.id,
              firstName: user.firstName,
              lastName: user.lastName,
              isOnline: user.isOnline,
              mobileNumber: user.mobileNumber,
              email: user.email
            });
          }
        }
      });

      response.status(200).json({ data: userResult });
    } else {
      response.status(400).json({ status: "error 400" });
    }
  });
});
