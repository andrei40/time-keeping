import { auth, firestore } from "../firebase";
import { User, UserTimeHistory } from "../interface";

export const signInUser = async (email: string, password: string) => {
  auth.signInWithEmailAndPassword(email, password).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    if (errorCode === "auth/wrong-password") {
      alert("Wrong password.");
    } else {
      alert(errorMessage);
    }
  });
};

export const signUpUser = async (
  firstName: string,
  lastName: string,
  mobileNumber: string,
  email: string,
  password: string
) => {
  const authUser: firebase.auth.UserCredential = await auth.createUserWithEmailAndPassword(
    email,
    password
  );

  const userUId = !!authUser.user ? authUser.user.uid : "";

  const createUserRoleResult = await firestore.collection("roles").add({
    userUId: userUId,
    role: "employee"
  });

  await firestore
    .collection("users")
    .doc(userUId)
    .set({
      email,
      firstName,
      lastName,
      mobileNumber
    });

  return { user: userUId, role: createUserRoleResult };
};

export const getOnlineUsers = async (callback = (users: User[]) => {}) => {
  firestore
    .collection("users")
    .where("isOnline", "==", true)
    .onSnapshot(querySnapshot => {
      if (querySnapshot.docs.length > 0) {
        let users: any = [];
        querySnapshot.docs.forEach(doc => {
          if (doc.exists) {
            users.push(doc.data());
          }
        });

        callback(users);
      } else {
        callback([]);
      }
    });
};

export const addUserTimeHistory = async (
  userUId: string,
  isClockIn: boolean,
  datetime: string
) => {
  await firestore
    .collection("user_time")
    .doc()
    .set({
      userUId: userUId,
      clockEvent: isClockIn ? "clock out" : "clock in",
      userDateTime: datetime
    });

  return { status: "ok" };
};

export const getUserTimeHistory = async (userUId: string) => {
  let timeHistory: UserTimeHistory[] = [];
  await firestore
    .collection("user_time")
    .where("userUId", "==", userUId)
    .get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        timeHistory.push({
          userUId: doc.data().userUId,
          clockEvent: doc.data().clockEvent,
          userDateTime: doc.data().userDateTime
        });
      });
    })
    .catch(err => {
      console.log("Error getting documents", err);
    });
  return timeHistory;
};
export const updateUserProfile = async (
  userUId: string,
  firstName: string,
  lastName: string,
  mobileNumber: string,
  email: string
) => {
  let user: any;
  await getUser(userUId).then(data => {
    user = data;
  });
  await firestore
    .collection("users")
    .doc(userUId)
    .set({
      email: user.email === email ? user.email : email,
      firstName: user.firstName === firstName ? user.firstName : firstName,
      lastName: user.lastName === lastName ? user.lastName : lastName,
      mobileNumber:
        user.mobileNumber === mobileNumber ? user.mobileNumber : mobileNumber,
      isOnline: user.isOnline
    });

  return { status: "ok" };
};

export const setUserOnline = async (userUId: string) => {
  console.log("setUserOnline");
  let user: any;
  await getUser(userUId).then(data => {
    user = data;
  });
  await firestore
    .collection("users")
    .doc(userUId)
    .set({
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      mobileNumber: user.mobileNumber,
      isOnline: true
    });
  return { status: "ok" };
};

export const setUserOffline = async (userUId: string) => {
  console.log("setUserOffline");
  let user: any;
  await getUser(userUId).then(data => {
    user = data;
  });

  await firestore
    .collection("users")
    .doc(userUId)
    .set({
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      mobileNumber: user.mobileNumber,
      isOnline: false
    });
  return { status: "ok" };
};

export const signOut = async (userUId: string) => {
  auth.signOut();
  setUserOffline(userUId);
};

export const getUser = async (userUId: string) => {
  let user;
  // console.log(userUId);
  await firestore
    .collection("users")
    .doc(userUId)
    .get()
    .then(doc => {
      if (doc.exists) {
        user = doc.data();
      } else {
        console.log("No such document!");
      }
    })
    .catch(err => {
      console.log("Error getting documents", err);
    });
  return user;
};

export const getAllUsers = async () => {
  let users: User[] = [];
  await firestore
    .collection("users")
    .get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        users.push({
          userUId: doc.id,
          firstName: doc.data().firstName,
          lastName: doc.data().lastName,
          mobileNumber: doc.data().mobileNumber,
          email: doc.data().email,
          isOnline: doc.data().isOnline
        });
      });
    })
    .catch(err => {
      console.log("Error getting documents", err);
    });
  return users;
};
