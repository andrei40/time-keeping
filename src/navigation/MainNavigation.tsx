import React, { Component } from "react";
import { IonReactRouter } from "@ionic/react-router";
import { Route, Redirect } from "react-router";

import { auth } from "../firebase/index";
import SignUp from "../pages/SignUp/SignUp";
import { setUserOnline } from "../services/user";
import Tabs from "./Tabs";
import SignIn from "../pages/SignIn/SignIn";

import { checkRole } from "../api/permission";

interface OwnState {
  isAuthed: boolean;
  userUId: string;
  role: string;
}

class MainNavigation extends Component<any, OwnState> {
  public state = {
    isAuthed: false,
    userUId: "",
    role: ""
  };
  public componentDidMount = () => {
    auth.onAuthStateChanged(async (user: firebase.User | null) => {
      if (user) {
        setUserOnline(user.uid);
        try {
          const role = await checkRole(user.uid);
          this.setState({
            isAuthed: true,
            userUId: user.uid,
            role: role.data.role
          });
        } catch (error) {
          console.log("error, ", error);
          auth.signOut();
          this.setState({ isAuthed: false });
        }
      } else {
        auth.signOut();
        this.setState({ isAuthed: false });
      }
    });
  };

  public render() {
    const { isAuthed, userUId, role } = this.state;
    return (
      <IonReactRouter>
        {isAuthed && <Tabs userUId={userUId} isAuthed={isAuthed} role={role} />}
        {!isAuthed && <Redirect to="/sign-in" />}
        <Route
          path="/sign-in"
          render={props => {
            return isAuthed ? (
              <Redirect to="/profile" />
            ) : (
              <SignIn isAuthed={isAuthed} {...props} />
            );
          }}
          exact={true}
        />
        <Route path="/sign-up" component={SignUp} exact={true} />
      </IonReactRouter>
    );
  }
}

export default MainNavigation;
