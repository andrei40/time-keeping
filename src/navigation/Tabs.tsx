import React, { Component } from "react";
import {
  IonTabs,
  IonTabBar,
  IonTabButton,
  IonLabel,
  IonRouterOutlet,
  IonPage,
  IonContent
} from "@ionic/react";
import { Route } from "react-router";
import Profile from "../pages/Profile/Profile";
import Employee from "../pages/Employee/Employee";
import TestHeader from "../pages/components/header";
import TimeHistory from "../pages/Profile/TimeHistory";
import EmployeeDetails from "../pages/Employee/EmployeeDetails";

interface OwnProps {
  userUId: string;
  isAuthed: boolean;
  role: string;
}

class Tabs extends Component<OwnProps> {
  public render() {
    const { userUId, role } = this.props;
    return (
      <IonTabs>
        <IonRouterOutlet>
          <Route
            path="/profile"
            exact
            render={props => {
              return <Profile userUId={userUId} />;
            }}
          />
          <Route
            path="/employee/details"
            exact
            render={props => {
              return <EmployeeDetails userUId={userUId} />;
            }}
          />
          <Route
            path="/time-history"
            exact
            render={props => {
              return <TimeHistory userUId={userUId} />;
            }}
          />
          <Route
            path="/employee"
            exact
            render={props => {
              return <Employee />;
            }}
          />
          <Route
            path="/test"
            exact
            render={props => {
              return (
                <IonPage>
                  <TestHeader />
                  <IonContent className="ion-padding">
                    <IonLabel>TEST</IonLabel>
                  </IonContent>
                </IonPage>
              );
            }}
          />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="profile" href="/profile">
            <IonLabel>Profile</IonLabel>
          </IonTabButton>
          <IonTabButton
            tab="employee"
            disabled={role === "admin" ? false : true} // initial permistion checking this could be redesign: create a list of roles
            href="/employee"
          >
            <IonLabel>Employee</IonLabel>
          </IonTabButton>
          <IonTabButton tab="test" href="/test">
            <IonLabel>Test</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    );
  }
}
export default Tabs;
