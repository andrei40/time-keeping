export interface User {
  userUId?: string;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  isOnline: boolean;
}

export interface UserTimeHistory {
  userUId: string;
  clockEvent: string;
  userDateTime: string;
}
