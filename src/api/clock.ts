import rp from "request-promise";

// import { functions } from "../firebase/index";

const CLOUD_FUNCTIONS_URL =
  "http://localhost:5000/time-keeping-6dc0c/us-central1/";

export const getClock = async () => {
  console.log("getClockTime");
  return await rp({
    method: "GET",
    url: CLOUD_FUNCTIONS_URL + "getClockTime",
    headers: {
      "Content-Type": "application/json"
    },
    json: true
  });
};
