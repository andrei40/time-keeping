import rp from "request-promise";

// TODO: initial permission just to check user role, can be redesign to create a list of role

const CLOUD_FUNCTIONS_URL =
  "http://localhost:5000/time-keeping-6dc0c/us-central1/";

export const getEmployees = async () => {
  console.log("getEmployees api");
  return await rp({
    method: "GET",
    url: CLOUD_FUNCTIONS_URL + "getEmployees",
    headers: {
      "Content-Type": "application/json"
    },
    json: true
  });
};
