import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";

const firebaseConfig = {
  apiKey: "AIzaSyBOI9LSsNN0vF_ONzLhAZCL-0LwZpsNrxs",
  authDomain: "time-keeping-6dc0c.firebaseapp.com",
  databaseURL: "https://time-keeping-6dc0c.firebaseio.com",
  projectId: "time-keeping-6dc0c",
  storageBucket: "time-keeping-6dc0c.appspot.com",
  messagingSenderId: "638060209534",
  appId: "1:638060209534:web:4cbc6e244f03f697e27f2f",
  measurementId: "G-LTJHJJTK1P"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const functions = firebase.functions();
