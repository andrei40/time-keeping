import React, { Component } from "react";
import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonPage,
  IonToolbar,
  IonTitle,
  IonContent
} from "@ionic/react";
import { UserTimeHistory } from "../../interface";
import ProfileForm from "../components/profileForm";

interface OwnProps {
  userUId: string;
}
interface OwnState {
  userTimeHistory: UserTimeHistory[];
}

class EmployeeDetails extends Component<OwnProps, OwnState> {
  public state = {
    userTimeHistory: []
  };
  public render() {
    const { userUId } = this.props;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonBackButton defaultHref="/profile" />
            </IonButtons>
            <IonTitle>Employee Details</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <p>Employee #: {userUId}</p>
          <ProfileForm userUId={userUId} />
        </IonContent>
      </IonPage>
    );
  }
}

export default EmployeeDetails;
