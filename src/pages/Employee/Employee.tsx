import {
  IonList,
  IonItemSliding,
  IonLabel,
  IonItem,
  IonContent,
  IonPage,
  withIonLifeCycle,
  IonButton
} from "@ionic/react";
import React, { Component } from "react";
import * as _ from "lodash";

import { getOnlineUsers } from "../../services/user";
import TestHeader from "../components/header";
import { User } from "../../interface";
import { getEmployees } from "../../api/employee";

interface OwnProps {}

interface OwnState {
  users: User[];
  employees: User[];
}

class Employee extends Component<OwnProps, OwnState> {
  public state = {
    users: [],
    employees: []
  };
  componentDidMount() {
    getOnlineUsers(users => {
      this.setState({
        users
      });
    });
    this.getEmployee();
  }

  public getEmployee = async () => {
    const employees = await getEmployees();
    this.setState({ employees: employees.data });
  };

  public render() {
    const { users, employees } = this.state;
    return (
      <IonPage>
        <TestHeader />
        <IonContent className="ion-padding">
          Employee List
          {!_.isEmpty(employees) && (
            <IonList>
              <IonItemSliding>
                {employees.map((value: User, index) => {
                  return (
                    <IonItem key={index}>
                      <IonButton routerLink={"/employee/details"} fill="clear">
                        {value.firstName} {value.lastName}
                      </IonButton>
                    </IonItem>
                  );
                })}
              </IonItemSliding>
            </IonList>
          )}
        </IonContent>
        <IonContent className="ion-padding">
          Online User
          {users && (
            <IonList>
              <IonItemSliding>
                {users.map((value: User, index) => {
                  return (
                    <IonItem key={index}>
                      <IonLabel>
                        {value.firstName} {value.lastName}
                      </IonLabel>
                    </IonItem>
                  );
                })}
              </IonItemSliding>
            </IonList>
          )}
        </IonContent>
      </IonPage>
    );
  }
}

export default withIonLifeCycle(Employee);
