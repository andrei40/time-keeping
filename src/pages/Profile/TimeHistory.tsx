import React, { Component } from "react";
import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonPage,
  IonToolbar,
  IonTitle,
  IonContent,
  IonList,
  IonItemSliding,
  IonItem,
  IonLabel
} from "@ionic/react";
import { UserTimeHistory } from "../../interface";
import { getUserTimeHistory } from "../../services/user";

interface OwnProps {
  userUId: string;
}
interface OwnState {
  userTimeHistory: UserTimeHistory[];
}

class TimeHistory extends Component<OwnProps, OwnState> {
  public state = {
    userTimeHistory: []
  };
  componentDidMount() {
    console.log("view history");
    getUserTimeHistory(this.props.userUId).then(res => {
      console.log("this is res -- ", res);
      this.setState({ userTimeHistory: res });
    });
  }
  public render() {
    const { userTimeHistory } = this.state;
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonBackButton defaultHref="/profile" />
            </IonButtons>
            <IonTitle>Time History</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <p>TimeHistory</p>
          {userTimeHistory && (
            <IonList>
              <IonItemSliding>
                {userTimeHistory.map((value: any, index) => {
                  console.log(value);
                  return (
                    <IonItem key={index}>
                      <IonLabel>
                        {value.clockEvent}:{value.userDateTime}
                      </IonLabel>
                    </IonItem>
                  );
                })}
              </IonItemSliding>
            </IonList>
          )}
        </IonContent>
      </IonPage>
    );
  }
}

export default TimeHistory;
