import { IonContent, IonButton, IonPage } from "@ionic/react";
import React, { Component } from "react";
import { signOut } from "../../services/user";
import TestHeader from "../components/header";
import Clock from "../components/clock";
import ProfileForm from "../components/profileForm";

interface OwnProps {
  userUId: string;
}
class Profile extends Component<OwnProps> {
  public signOut = () => {
    signOut(this.props.userUId);
  };

  public render() {
    const { userUId } = this.props;
    return (
      <IonPage>
        <TestHeader />
        <IonContent>
          <ProfileForm userUId={userUId} />
          <Clock userUId={userUId} />
          <IonButton expand="block" fill="clear" routerLink="/time-history">
            View Time History
          </IonButton>
          <IonButton expand="block" fill="clear" onClick={this.signOut}>
            Sign Out
          </IonButton>
        </IonContent>
      </IonPage>
    );
  }
}

export default Profile;
