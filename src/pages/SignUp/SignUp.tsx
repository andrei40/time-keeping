import React, { Component } from "react";
import {
  IonButton,
  IonCard,
  IonItem,
  IonLabel,
  IonInput,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonContent,
  IonPage
} from "@ionic/react";

import { signUpUser } from "../../services/user";
import TestHeader from "../components/header";

interface OwnProps {
  history: any;
}
interface OwnState {
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  password: string;
}
class SignUp extends Component<OwnProps, OwnState> {
  public state = {
    firstName: "",
    lastName: "",
    mobileNumber: "",
    email: "",
    password: ""
  };
  public render() {
    const { history } = this.props;
    return (
      <IonPage>
        <TestHeader />
        <IonContent>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle className="ion-text-center">Sign Up</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <form
                onSubmit={event => {
                  event.preventDefault();
                  signUpUser(
                    this.state.firstName,
                    this.state.lastName,
                    this.state.mobileNumber,
                    this.state.email,
                    this.state.password
                  );
                  history.push("/sign-in");
                }}
              >
                <IonItem>
                  <IonLabel position="floating">First Name</IonLabel>
                  <IonInput
                    required
                    name="firstName"
                    type="text"
                    value={this.state.firstName}
                    onIonChange={event => {
                      this.setState({
                        firstName: event.detail.value as string
                      });
                    }}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Last Name</IonLabel>
                  <IonInput
                    required
                    name="lastName"
                    type="text"
                    value={this.state.lastName}
                    onIonChange={event => {
                      this.setState({ lastName: event.detail.value as string });
                    }}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Mobile Number</IonLabel>
                  <IonInput
                    required
                    name="mobileNumber"
                    type="text"
                    value={this.state.mobileNumber}
                    onIonChange={event => {
                      this.setState({
                        mobileNumber: event.detail.value as string
                      });
                    }}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Email</IonLabel>
                  <IonInput
                    required
                    name="email"
                    type="email"
                    value={this.state.email}
                    onIonChange={event => {
                      this.setState({ email: event.detail.value as string });
                    }}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Password</IonLabel>
                  <IonInput
                    required
                    name="password"
                    type="password"
                    value={this.state.password}
                    onIonChange={event => {
                      this.setState({ password: event.detail.value as string });
                    }}
                  ></IonInput>
                </IonItem>
                <IonButton type="submit" expand="block" fill="clear">
                  Submit
                </IonButton>
              </form>
              <IonButton expand="block" fill="clear" routerLink="/sign-in">
                Cancel
              </IonButton>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonPage>
    );
  }
}

export default SignUp;
