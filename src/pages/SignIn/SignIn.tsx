import React, { Component } from "react";
import {
  IonButton,
  IonCard,
  IonItem,
  IonLabel,
  IonInput,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonContent,
  IonPage
} from "@ionic/react";

import { signInUser } from "../../services/user";
import TestHeader from "../components/header";

interface OwnProps {
  isAuthed: boolean;
  history: any;
}
interface OwnState {
  email: string;
  password: string;
}
class SignIn extends Component<OwnProps, OwnState> {
  public state = {
    email: "",
    password: ""
  };

  public signIn = (email: string, password: string) => {
    signInUser(email, password);
  };

  public render() {
    return (
      <IonPage>
        <TestHeader />
        <IonContent>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle className="ion-text-center">Sign In</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <form
                onSubmit={event => {
                  event.preventDefault();
                  this.signIn(this.state.email, this.state.password);
                }}
              >
                <IonItem>
                  <IonLabel position="floating">Email</IonLabel>
                  <IonInput
                    required
                    name="email"
                    type="email"
                    value={this.state.email}
                    onIonChange={event => {
                      this.setState({ email: event.detail.value as string });
                    }}
                  ></IonInput>
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Password</IonLabel>
                  <IonInput
                    required
                    name="password"
                    type="password"
                    value={this.state.password}
                    onIonChange={event => {
                      this.setState({ password: event.detail.value as string });
                    }}
                  ></IonInput>
                </IonItem>
                <IonButton type="submit" expand="block" fill="clear">
                  Sign In
                </IonButton>
              </form>
              <IonButton expand="block" fill="clear" routerLink="/sign-up">
                Sign Up
              </IonButton>
              <IonButton expand="block" fill="clear">
                Forgot Password?
              </IonButton>
            </IonCardContent>
          </IonCard>
        </IonContent>
      </IonPage>
    );
  }
}

export default SignIn;
