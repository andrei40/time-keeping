import {
  IonLabel,
  IonButton,
  IonItem,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonInput,
  IonToast
} from "@ionic/react";
import React, { Component } from "react";
import { getUser, signOut, updateUserProfile } from "../../services/user";
import { User } from "../../interface";

interface OwnProps {
  userUId: string;
}

interface OwnState {
  firstName: string;
  lastName: string;
  mobileNumber: string;
  email: string;
  user: User[];
  isEdit: boolean;
  showToast: boolean;
}

class ProfileForm extends Component<OwnProps, OwnState> {
  public state = {
    user: [],
    firstName: "",
    lastName: "",
    mobileNumber: "",
    email: "",
    isEdit: false,
    showToast: false
  };
  public signOut = () => {
    signOut(this.props.userUId);
  };
  componentDidMount() {
    getUser(this.props.userUId).then((user: User | undefined) => {
      if (user) {
        this.setState({
          firstName: user.firstName,
          lastName: user.lastName,
          mobileNumber: user.mobileNumber,
          email: user.email
        });
      }
    });
  }

  public render() {
    const {
      firstName,
      lastName,
      mobileNumber,
      email,
      isEdit,
      showToast
    } = this.state;
    return (
      <IonCard>
        <IonCardHeader>
          <IonCardTitle className="ion-text-center">Profile</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          <form
            onSubmit={event => {
              event.preventDefault();
              updateUserProfile(
                this.props.userUId,
                this.state.firstName,
                this.state.lastName,
                this.state.mobileNumber,
                this.state.email
              );
              this.setState({ showToast: !showToast, isEdit: !isEdit });
            }}
          >
            <IonItem>
              <IonLabel>First Name:</IonLabel>
              <IonInput
                disabled={!isEdit}
                name="firstName"
                type="text"
                value={firstName}
                onIonChange={event => {
                  this.setState({
                    firstName: event.detail.value as string
                  });
                }}
              ></IonInput>
            </IonItem>

            <IonItem>
              <IonLabel>Last Name:</IonLabel>
              <IonInput
                disabled={!isEdit}
                name="lastName"
                type="text"
                value={lastName}
                onIonChange={event => {
                  this.setState({ lastName: event.detail.value as string });
                }}
              ></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel>Mobile #:</IonLabel>
              <IonInput
                disabled={!isEdit}
                name="mobileNumber"
                type="text"
                value={mobileNumber}
                onIonChange={event => {
                  this.setState({
                    mobileNumber: event.detail.value as string
                  });
                }}
              ></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel>Email:</IonLabel>
              <IonInput
                disabled={!isEdit}
                name="email"
                type="email"
                value={email}
                onIonChange={event => {
                  this.setState({ email: event.detail.value as string });
                }}
              ></IonInput>
            </IonItem>

            {isEdit && (
              <IonButton type="submit" expand="block" fill="clear">
                Save
              </IonButton>
            )}
          </form>
          <IonButton
            expand="block"
            fill="clear"
            onClick={() => {
              this.setState({ isEdit: !isEdit });
            }}
          >
            {isEdit ? "Cancel" : "Edit"}
          </IonButton>
          <IonToast
            isOpen={showToast}
            onDidDismiss={() => this.setState({ showToast: !showToast })}
            message="Your profile have been saved."
            duration={200}
          />
        </IonCardContent>
      </IonCard>
    );
  }
}

export default ProfileForm;
