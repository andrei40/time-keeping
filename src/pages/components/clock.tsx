import {
  IonLabel,
  IonButton,
  IonItem,
  IonCard,
  IonCardContent
} from "@ionic/react";
import React, { Component } from "react";
import moment from "moment-timezone";

import { getClock } from "../../api/clock";
import { addUserTimeHistory } from "../../services/user";

interface OwnProps {
  userUId: string;
}

interface OwnState {
  time: string | null;
  date: string | null;
  timeZone: string;
  isClockIn: boolean;
}

class Clock extends Component<OwnProps, OwnState> {
  public state = {
    time: null,
    date: null,
    timeZone: "",
    isClockIn: false
  };
  componentDidMount() {
    getClock().then(response => {
      setInterval(() => this.tick(), 1000);
      this.setState({ timeZone: response.timezone });
    });
  }

  tick = () => {
    this.setState({
      time: moment()
        .tz(this.state.timeZone)
        .format("h:mm:ss a"),
      date: moment()
        .tz(this.state.timeZone)
        .format("dddd, MMMM Do YYYY")
    });
  };

  public render() {
    const { isClockIn } = this.state;
    return (
      <IonCard>
        <IonCardContent>
          <form
            onSubmit={event => {
              event.preventDefault();
              const datetime = this.state.date + " " + this.state.time;
              addUserTimeHistory(
                this.props.userUId,
                this.state.isClockIn,
                datetime
              );
            }}
          >
            <IonItem color={isClockIn ? "success" : "dark"}>
              <IonLabel className="ion-text-center">{this.state.time}</IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel className="ion-text-center">{this.state.date}</IonLabel>
            </IonItem>

            <IonButton
              type="submit"
              expand="block"
              color={isClockIn ? "success" : "dark"}
              onClick={() => {
                this.setState({ isClockIn: !this.state.isClockIn });
              }}
            >
              {isClockIn ? "Clock Out" : "Clock In"}
            </IonButton>
          </form>
        </IonCardContent>
      </IonCard>
    );
  }
}

export default Clock;
