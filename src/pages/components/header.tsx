import { IonHeader, IonTitle, IonToolbar } from "@ionic/react";
import React, { Component } from "react";

interface OwnProps {}

class TestHeader extends Component<OwnProps, any> {
  public render() {
    return (
      <IonHeader>
        <IonToolbar>
          <IonTitle>Testing 123</IonTitle>
        </IonToolbar>
      </IonHeader>
    );
  }
}

export default TestHeader;
